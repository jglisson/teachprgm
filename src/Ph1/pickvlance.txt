/PROG  PICKVLANCE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1044;
CREATE		= DATE 17-09-27  TIME 11:53:20;
MODIFIED	= DATE 17-09-27  TIME 11:53:20;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 15;
MEMORY_SIZE	= 1356;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !FANUC America Corp. ;
   2:  !ROBOGUIDE Generated This TPP ;
   3:  !Run SimPRO.cf to setup frame and ;
   4:  	UTOOL_NUM[GP1]=1 ;
   5: 	UFRAME_NUM[GP1]=6 ;
   6:	PR[6,3] = AR[1] * R[4];
   7:	J P[1] 25% FINE ACC50    ;
   8:	L P[2] 2000mm/sec CNT10    ;
   9:	L P[3] 2000mm/sec CNT10    ;
  10:	L P[4] 2000mm/sec FINE Offset, PR[6]    ;
  11:	L P[5] 100mm/sec FINE Offset, PR[6]    ;
  13:  	WAIT 1.00 (sec) ;
  14:	L P[6] 100mm/sec FINE    Offset, PR[6]    ;
  15:	L P[7] 2000mm/sec CNT100    ;
  17:	J P[8] 67% FINE    ;
/POS
P[1]{
   GP1:
	UF : 6, UT : 1,	
	J1=    90.000 deg,	J2=   -18.930 deg,	J3=   -58.670 deg,
	J4=     2.940 deg,	J5=    58.630 deg,	J6=   181.040 deg
};
P[2]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =   376.980  mm,	Y =   346.990  mm,	Z =    -5.730  mm,
	W =     -.070 deg,	P =    -2.570 deg,	R =     2.510 deg
};
P[3]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =   192.320  mm,	Y =  -208.640  mm,	Z =   -14.450  mm,
	W =     -.070 deg,	P =    -2.570 deg,	R =     2.510 deg
};
P[4]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =     0.000  mm,	Y =  -250.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[5]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[6]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =     0.000  mm,	Y =     0.000  mm,	Z =   200.000  mm,
	W =     0.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[7]{
   GP1:
	UF : 6, UT : 1,		CONFIG : 'F U T, 0, 0, 1',
	X =  1000.000  mm,	Y =     0.000  mm,	Z =   200.000  mm,
	W =     0.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[8]{
   GP1:
	UF : 6, UT : 1,	
	J1=    90.000 deg,	J2=   -18.930 deg,	J3=   -58.670 deg,
	J4=     2.940 deg,	J5=    58.630 deg,	J6=   181.040 deg
};
/END
