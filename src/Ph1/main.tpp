# MAIN
# =======
# Author: John Glisson

#===================DECLARATIONS=================================
# variable definitions
#Integers
RobotState				:= R[99]

#Constants

#SM states
SM_STARTUP				:= 1
SM_COMM_INIT			:= 2
SM_IDLE					:= 3
SM_MOVE_PERCH			:= 4
SM_PERCH				:= 5
SM_CAMERA_CALIBRATE		:= 6
SM_CAMERA_FIND_VALVE	:= 7
SM_LANCE_POSITION		:= 8
SM_LANCE_BURN_OPEN		:= 9
SM_PLACE_SHROUD			:= 10
SM_FAULT				:= 11
SM_JOG_INTO_ENVELOPE	:= 12

#UFRAME enumerations
UF_WORLD				:= 1
UF_TOOL					:= 2

#UTOOL enumerations



#Hardcoded Values
#TODO:Maybe these should be R[x] instead of hardcoded? Hardcode Min/max
TO_BURN_ERROR_MS		:= 60000
TO_OXYGEN_LOWFLOW_MS	:= 10000
DEFAULTSPEED			:= 250
CNT_VAL					:= 50


#Namespaced Variables
#Note: PR[1] May not be used, as it is used internally!
namespace Positions
	Perch				:= PR[2]
	LancePick			:= PR[3]
	CameraPick			:= PR[4]
	Service				:= PR[5]
	Safe				:= PR[6]
end

namespace Alarms
	ExternalFault 		:= UALM[1] #Control circuit de-energized outside of Robot Controller's scope
	CommError			:= UALM[2] #Generic catch-all error for when the controller is unable to communicate
	JogRequired 		:= UALM[3] #Programmed movement can't execute due to robot position/orientation mismatch
	CollisionDetected	:= UALM[4] #CollisionGuarding/Overtorque Condition
	ComLoss				:= UALM[5]
	BurnOpenTimeout		:= UALM[6]
	CamCalibError		:= UALM[7]
	ValveNotFound		:= UALM[8]
end

namespace Inputs
	inputHeartbeat 		:= DI[1]
	ErrAck				:= DI[2]
	ExitProgram			:= DI[3]  #NOTE: Also used in FaultInterrupt (need some sort of strrep() batch instruction)
end

namespace Outputs
	AtPerch 			:= DO[1]
	CameraAttached		:= DO[2]
	LanceAttached		:= DO[3]
	outputHeartbeat		:= DO[4]
end

namespace Commands
	tmp_jump			:= DI[8]
end

#===================/DECLARATIONS=================================


#====================MAIN PROGRAM LOGIC================================

RobotState = SM_STARTUP
eval "MESSAGE[Program Start...]"
#eval "MONITOR FaultInterrupt"


#Master State Machine
@MasterStateMachine

case RobotState
	when SM_STARTUP
		jump_to @startup
	when SM_COMM_INIT
		jump_to @CommInit
	when SM_IDLE
		jump_to @Idle
	when SM_MOVE_PERCH
		jump_to @MovePerch
	when SM_PERCH
		jump_to @WaitAtPerch
	when SM_CAMERA_CALIBRATE
		jump_to @CameraCalibration
	when SM_CAMERA_FIND_VALVE
		jump_to @FindValve
	when SM_LANCE_POSITION
		jump_to @MoveLance
	when SM_LANCE_BURN_OPEN
		jump_to @BurnOpen
	when SM_PLACE_SHROUD
		jump_to @PlaceShroud
	when SM_FAULT
		jump_to @FaultState
	when SM_JOG_INTO_ENVELOPE
		jump_to @ManualJog
end

#====================/MAIN PROGRAM LOGIC================================


#SM Jumps
#	Note: To "loop" correctly, must call "jump_to @MasterStateMachine" to "return".

#================ROBOT STARTUP====================================
@startup

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/ROBOT STARTUP====================================


#====================================================
@CommInit

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================


#====================================================
@Idle

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================


#====================================================
@MovePerch

joint_move.to(Positions::Perch).at(100, '%').term(CNT_VAL)

jump_to @MasterStateMachine
#================/====================================


#====================================================
@WaitAtPerch

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================


#====================================================
@CameraCalibration

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================


#====================================================
@FindValve

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================

#====================================================
@MoveLance

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================

#====================================================
@BurnOpen

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================

#====================================================
@PlaceShroud

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================

#====================================================
@FaultState

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================

#====================================================
@ManualJog

if Commands::tmp_jump
	RobotState = SM_IDLE
end

jump_to @MasterStateMachine
#================/====================================



#Program End
@ProgramEnd
eval "MONITOR END FaultInterrupt"
