/PROG  PKPLCREG
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Simulation-only file.  Populate registers used in simulation program.";
PROG_SIZE	= 928;
CREATE		= DATE 17-10-12  TIME 08:18:08;
MODIFIED	= DATE 17-10-12  TIME 08:18:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 13;
MEMORY_SIZE	= 1216;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
	2:	PR[10:HOME]					= P[10];
	3:	PR[11:BOW]					= P[11];
	4:	PR[12:PPW]					= P[12];
	5:	PR[13:ATLCAL]				= P[13];
	6:	PR[14:AVLCAL]				= P[14];
	7:	PR[15:BRNAPRCH]				= P[15];
	8:	!Tool Retrieval/Replacement operations
	18:	PR[31:TLRMVL1 APPROACH] 	= P[31];
	19:	PR[32:TLRMVL2 ATTACH] 		= P[32];
	20:	PR[33:TLRMVL3 UP1] 			= P[33];
	21:	PR[34:TLRMVL4 OUT1] 		= P[34];
	22:	PR[35:TLRMVL5 UP2] 			= P[35];
	23:	PR[36:TLRMVL6 LEFT EDGE] 	= P[36];
	24:	PR[37:TLRMVL7 RETRACT] 		= P[37];
	2:	PR[38:TLRMVL8 RESVD] 		= P[38];
	4:	!Extra steps for lance removal
	25:	PR[39:LNCRMVL1] 			= P[39];
	25:	PR[40:LNCRMVL2] 			= P[40];
	25:	PR[41:LNCRMVL3] 			= P[41];
/POS
P[10]{
   GP1:
	UF : 0, UT : 1,	
	J1=     0.000 deg,	J2=     0.000 deg,	J3=     0.000 deg,
	J4=     0.000 deg,	J5=     0.000 deg,	J6=     0.000 deg
};
P[11]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   119.690  mm,	Y =  -482.620  mm,	Z = -1950.700  mm,
	W =    75.000 deg,	P =    15.000 deg,	R =    45.000 deg
};
P[12]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =  1612.170  mm,	Y =   786.660  mm,	Z =   545.000  mm,
	W =   180.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[13]{
   GP1:
	UF : 6, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =   180.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[14]{
   GP1:
	UF : 7, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[15]{
   GP1:
	UF : 7, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  -500.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[31]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =  -500.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[32]{
   GP1:
	UF :0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[33]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   300.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[34]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   300.000  mm,	Y =     0.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[35]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =     0.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[36]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  -300.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[37]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   618.340  mm,	Y =  -300.000  mm,	Z = -1639.880  mm,
	W =     0.000 deg,	P =    22.320 deg,	R =     0.000 deg
};
P[38]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   119.690  mm,	Y =  -482.620  mm,	Z = -1950.700  mm,
	W =    75.000 deg,	P =    15.000 deg,	R =    45.000 deg
};
P[39]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  0.000  mm,	Z = -250.000  mm,
	W =    0.000 deg,	P =    0.000 deg,	R =    0.000 deg
};
P[40]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  0.000  mm,	Z = -750.000  mm,
	W =    0.000 deg,	P =    45.000 deg,	R =    0.000 deg
};
P[41]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  -600.000  mm,	Z = -750.000  mm,
	W =    0.000 deg,	P =    45.000 deg,	R =    0.000 deg
};
/END
