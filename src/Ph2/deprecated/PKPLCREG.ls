/PROG  PKPLCREG_old
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Simulation-only file.  Populate registers used in simulation program.";
PROG_SIZE	= 928;
CREATE		= DATE 17-10-12  TIME 08:18:08;
MODIFIED	= DATE 17-10-12  TIME 08:18:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 13;
MEMORY_SIZE	= 1216;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
	1:  LINEAR_MAX_SPEED=3000;
	2:	!PR[10:HOME]						= P[11];
	2:	PR[10:HOME]						= P[20];
	3:	PR[11:BOW]						= P[8];
	4:	PR[12:PPW]						= P[13];
	5:	PR[13:ATLCAL]					= P[17];
	6:	PR[14:AVLCAL]					= P[19];
	7:	PR[15:BRNAPRCH]					= P[18];
	8:	!PR[16:ATCHTL1];
	9:	!PR[17:ATCHTL2];
	10:	!PR[18:ATCHTL3];
	11:	!PR[19:ATCHTL4];
	12:	!PR[20:ATCHTLSVC];
	13:	!PR[21:RTRCTTLA];
	14:	!PR[22:RTRCTTLB];
	15:	!PR[22:ATCHTLSVC];
	16:	!PR[22:RTRCTSVCA];
	17:	!PR[22:RTRCTSVCB];
	18:	PR[31:TLRMVL1 APPROACH] 		= P[1];
	19:	PR[32:TLRMVL2 ATTACH] 			= P[2];
	20:	PR[33:TLRMVL3 UP1] 				= P[3];
	21:	PR[34:TLRMVL4 OUT1] 			= P[4];
	22:	PR[35:TLRMVL5 UP2] 				= P[5];
	23:	PR[36:TLRMVL6 LEFT EDGE] 		= P[6];
	24:	PR[37:TLRMVL7 RETRACT] 			= P[7];
	25:		!PR[38:TLRMVL8] 				= P[8]; This is BOW position!
	25:	PR[39:LNCRMVL1] 				= P[9];
	25:	PR[40:LNCRMVL2] 				= P[10];
	25:	PR[41:LNCRMVL3] 				= P[11];
	25:	!PR[42:LNCRMVL4] 				= P[14];Not using any more
	25:	PR[44:CALIB APRCH 1] 			= P[15];
	25:	PR[45:CALIB APRCH 2] 			= P[16];
/POS
P[1]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =  -500.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[2]{
   GP1:
	UF :0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[3]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   300.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[4]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   300.000  mm,	Y =     0.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[5]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =     0.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[6]{
   GP1:
	UF : 0,UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  -300.000  mm,	Z =  -250.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[7]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   618.340  mm,	Y =  -300.000  mm,	Z = -1639.880  mm,
	W =     0.000 deg,	P =    22.320 deg,	R =     0.000 deg
};
P[8]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   119.690  mm,	Y =  -482.620  mm,	Z = -1950.700  mm,
	W =    75.000 deg,	P =    15.000 deg,	R =    45.000 deg
};
P[9]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  0.000  mm,	Z = -250.000  mm,
	W =    0.000 deg,	P =    0.000 deg,	R =    0.000 deg
};
P[10]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  0.000  mm,	Z = -750.000  mm,
	W =    0.000 deg,	P =    45.000 deg,	R =    0.000 deg
};
P[11]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   900.000  mm,	Y =  -600.000  mm,	Z = -750.000  mm,
	W =    0.000 deg,	P =    45.000 deg,	R =    0.000 deg
};
P[12]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =  1842.220  mm,	Y =     0.000  mm,	Z =    77.980  mm,
	W =   180.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[13]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =  1612.170  mm,	Y =   786.660  mm,	Z =   545.000  mm,
	W =   180.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[14]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =  900.000  mm,	Y =   -600.000  mm,	Z =   -750.000  mm,
	W =   0.000 deg,	P =   0.000 deg,	R =     0.000 deg
};
P[15]{
   GP1:
	UF : 6, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X = -2191.870  mm,	Y =   321.090  mm,	Z =  1206.950  mm,
	W =     -.490 deg,	P =   -43.080 deg,	R =  -110.750 deg
};
P[16]{
   GP1:
	UF : 6, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  -250.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =    90.000 deg,	P =   -90.000 deg,	R =    90.000 deg
};
P[17]{
   GP1:
	UF : 6, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =   180.000 deg,	P =   -90.000 deg,	R =     0.000 deg
};
P[18]{
   GP1:
	UF : 7, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =  -500.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[19]{
   GP1:
	UF : 7, UT : 2,		CONFIG : 'F U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =     0.000 deg,	P =     0.000 deg,	R =     0.000 deg
};
P[20]{
   GP1:
	UF : 0, UT : 1,	
	J1=     0.000 deg,	J2=     0.000 deg,	J3=     0.000 deg,
	J4=     0.000 deg,	J5=     0.000 deg,	J6=     0.000 deg
};
/END
