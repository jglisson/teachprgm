Nucor-Berkely Steel plant Robotics Application
Developers:
J. Glisson, Project Engineer, iAutomation, Inc.
G. Bjelcevic, Principal Engineer, iAutomation, Inc.

Introduction:
Nucor has contracted iAutomation Inc. to develop and commission a robotics application.
Pursuant to Phase 1 specifications detailed elsewhere regarding the functionality of the robot, this repository is meant to track all teach pendant program files, related documentation, and instructions.

Directory Structure:
/src: Teach Pendant Plus (*.tpp) and listing files (.ls)- programs to run the machine.  
/doc: All documentation applicable to this facet of the project
/roboguide: Fanuc Roboguide workcell export files (*.rgx)
/etc: Catch-all
/BuildFile.bat: BATCH file for Transpiling files /src->/bin

